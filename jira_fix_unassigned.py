from jira import JIRA
import creds as CREDS


servername = CREDS.server
options = {'server': servername}
user = CREDS.user
password = CREDS.password
jira = JIRA(options, basic_auth=(user, password))



list_of_issues = jira.search_issues('assignee = NULL AND created < -10d AND (status != done AND status != closed)')
#list_of_issues = jira.search_issues('issue=incpoc-522')


for issue in list_of_issues:
	print issue.key + " : " + issue.fields.reporter.name + " " + issue.fields.status.name + " : " + getattr(issue.fields.assignee, "name", "")
	
	# Add a comment stating that we're reassigning it
	# Reassign the ticket to the person who reported it
	#   if we fail, reassign to the user who is running this script

        print "[*] Adding comment: *This ticket is more than 10 days old and unassigned. Reassigning to Reporter* to " + str(issue.fields.reporter.name)
	jira.add_comment(issue, '*This ticket is more than 10 days old and unassigned. Reassigning to Reporter.*')

        print "[*] Trying to reassign Issue: " + str(issue.key) + " to " + issue.fields.reporter.name
        try:	
		jira.assign_issue(issue, issue.fields.reporter.name)
	except:
		jira.assign_issue(issue, user)
